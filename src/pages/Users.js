import { useContext, useEffect, useState } from 'react'
import { Navigate } from 'react-router-dom'
import { Container, Col, Row, Button, Modal, Table } from 'react-bootstrap'

import MakeUserAdmin from '../components/MakeUserAdmin'
import UserContext from '../UserContext'

export default function Users() {
  const { user } = useContext(UserContext)
  const [usersData, setUsersData] = useState([])
  const [users, setUsers] = useState([])
  const [selectedUser, setSelectedUser] = useState(null)
  const [showAddressModal, setShowAddressModal] = useState(false)

  const fetchAllUsers = async () => {
    fetch('https://capstone2-ganir.onrender.com/user/', {
      headers: {
        Authorization: `Bearer ${user.token}`
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setUsersData(data)
      })
  }

  useEffect(() => {
    fetchAllUsers()
  }, [])

  useEffect(() => {
    const usersArr = usersData.map((user, index) => {
      return (
        <tr key={user._id}>
        	<td>{index + 1}</td>
          	<td>{user._id}</td>
          	<td>{user.email}</td>
          	<td>{user.firstName} {user.lastName}</td>
          	<td>
            	<Button variant="info" onClick={() => handleShowAddressModal(user)}>
              		View Address
            	</Button>
          	</td>
          <td>
            <MakeUserAdmin
              userId={user._id}
              isAdmin={user.isAdmin}
              fetchAllUsers={fetchAllUsers}
            />
          </td>
        </tr>
      );
    });

    setUsers(usersArr);
  }, [usersData]);

  const handleShowAddressModal = (user) => {
    setSelectedUser(user);
    setShowAddressModal(true);
  }

  const handleCloseAddressModal = () => {
    setSelectedUser(null);
    setShowAddressModal(false);
  }

  console.log(users)

  return (
    (user.token === null) ? (
      <Navigate to="/login" />
    ) : (
      <>
        {usersData.length === 0 ? ( // Check if usersData is empty
          <p>Loading...</p>
        ) : (
          user.isAdmin ? (
            <>
              <div id='users'>
              <Container fluid className='p-3'>
              	<Row className='text-center'>
              		<h1>All Users</h1>
              	</Row>
                <Row>
                  <Col>
                    <Table striped bordered hover responsive className="mt-4">
                      <thead>
                        <tr className="text-center">
                        	<td>#</td>
                         	<th>User ID</th>
                         	<th>Email</th>
                         	<th>Full Name</th>
                          	<th>Address</th>
                          	<th colSpan="2">Actions</th>
                        </tr>
                      </thead>
                      <tbody className="text-center">{users}</tbody>
                    </Table>
                  </Col>
                </Row>
              </Container>
              </div>

              <Modal show={showAddressModal} onHide={handleCloseAddressModal}>
                <Modal.Header closeButton>
                  <Modal.Title>User Address</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  {selectedUser && (
                    <div>
                      <p>Region: {selectedUser.address.region}</p>
                      <p>Province: {selectedUser.address.province}</p>
                      <p>City: {selectedUser.address.city}</p>
                      <p>Barangay: {selectedUser.address.barangay}</p>
                      <p>Street Name: {selectedUser.address.streetName}</p>
                      <p>Building/House Number: {selectedUser.address.buildingHouseNumber}</p>
                    </div>
                  )}
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleCloseAddressModal}>
                    Close
                  </Button>
                </Modal.Footer>
              </Modal>
            </>
          ) : (
            <Navigate to="/home" />
          )
        )}
      </>
    )
  );
}

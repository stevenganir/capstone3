import React, { useState, useEffect } from 'react';
import { Navigate, Link } from 'react-router-dom'; 
import { Button, Row, Col } from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function RegistrationForm() {
  const [formData, setFormData] = useState({
    email: '',
    firstName: '',
    lastName: '',
    password: '',
    address: {
      region: '',
      province: '',
      city: '',
      barangay: '',
      streetName: '',
      buildingHouseNumber: '',
    },
  });

  const [confirmPassword, setConfirmPassword] = useState('')
  const [isActive, setIsActive] = useState(false) 
  const [isRegistered, setIsRegistered] = useState(false)

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleAddressChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      address: {
        ...prevData.address,
        [name]: value,
      },
    }));
  };

  const handleConfirmPasswordChange = (e) => {
    setConfirmPassword(e.target.value)
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('https://capstone2-ganir.onrender.com/user/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (response.status === 201) {
        const data = await response.json();

        setFormData({
    		email: '',
    		firstName: '',
   			lastName: '',
    		password: '',
    		address: {
      		region: '',
      		province: '',
      		city: '',
      		barangay: '',
      		streetName: '',
      		buildingHouseNumber: '',
    		},
  		})

        setConfirmPassword('');

        Swal.fire({
  			text: `${data.message}`,
  			icon: 'success',
  			})

		setIsRegistered(true)

      } else {
      	const data = await response.json();
        Swal.fire({
  			text: `${data.message}`,
  			icon: 'error'
		})
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  useEffect(()=>{

		if(
			(formData.email !== "" && 
			formData.firstName !== "" && 
			formData.lastName !== "" && 
			formData.address.region !=="" && 
			formData.address.province !==""  && 
			formData.address.city !=="" &&
			formData.address.barangay !=="" && 
			formData.address.streetName !=="" &&
			formData.address.buildingHouseNumber !=="" &&
			confirmPassword !=="") && 
			(formData.password === confirmPassword)){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[formData.email,formData.firstName,formData.lastName,formData.address.region,formData.address.province,formData.address.city,formData.address.barangay,formData.address.streetName,formData.address.buildingHouseNumber,confirmPassword])

  return (
  	(isRegistered === true) ?
  		<Navigate to="/login" />
  		:
	    <div id='registration-form' className="container-fluid p-3">
	      <h2 className='m-3 text-center'>Register</h2>
	      <Row className='text-center mt-2'>
        	<Col>
          	<p>
            	Already have an account?&nbsp;
            	<Link to='/register'>Log in</Link>
          	</p>
        	</Col>
      	</Row>
	      <form onSubmit={handleSubmit}>
	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="email" className="form-label">
	            Email
	          </label>
	          <input
	            type="email"
	            className="form-control"
	            id="email"
	            name="email"
	            placeholder="juandelacruz@gmail.com"
	            value={formData.email}
	            onChange={handleChange}
	            required
	          />
	        </div>
	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="firstName" className="form-label">
	            First Name
	          </label>
	          <input
	            type="text"
	            className="form-control"
	            id="firstName"
	            name="firstName"
	            placeholder="Juan"
	            value={formData.firstName}
	            onChange={handleChange}
	            required
	          />
	        </div>
	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="lastName" className="form-label">
	            Last Name
	          </label>
	          <input
	            type="text"
	            className="form-control"
	            id="lastName"
	            name="lastName"
	            placeholder="Dela Cruz"
	            value={formData.lastName}
	            onChange={handleChange}
	            required
	          />
	        </div>
	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="region" className="form-label">
	            Region
	          </label>
	          <input
	            type="text"
	            className="form-control"
	            id="region"
	            name="region"
	            placeholder="NCR"
	            value={formData.address.region}
	            onChange={handleAddressChange}
	            required
	          />
	        </div>
	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="province" className="form-label">
	            Province
	          </label>
	          <input
	            type="text"
	            className="form-control"
	            id="province"
	            name="province"
	            placeholder="Metro Manila"
	            value={formData.address.province}
	            onChange={handleAddressChange}
	            required
	          />
	        </div>
	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="city" className="form-label">
	            City
	          </label>
	          <input
	            type="text"
	            className="form-control"
	            id="city"
	            name="city"
	            placeholder="Manila City"
	            value={formData.address.city}
	            onChange={handleAddressChange}
	            required
	          />
	        </div>
	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="barangay" className="form-label">
	            Barangay
	          </label>
	          <input
	            type="text"
	            className="form-control"
	            id="barangay"
	            name="barangay"
	            placeholder="Brgy. 143"
	            value={formData.address.barangay}
	            onChange={handleAddressChange}
	            required
	          />
	        </div>
	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="streetName" className="form-label">
	            Street Name
	          </label>
	          <input
	            type="text"
	            className="form-control"
	            id="streetName"
	            name="streetName"
	            placeholder="Masigasig Street"
	            value={formData.address.streetName}
	            onChange={handleAddressChange}
	            required
	          />
	        </div>
	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="buildingHouseNumber" className="form-label">
	            Building/House Number
	          </label>
	          <input
	            type="text"
	            className="form-control"
	            id="buildingHouseNumber"
	            name="buildingHouseNumber"
	            placeholder='839'
	            value={formData.address.buildingHouseNumber}
	            onChange={handleAddressChange}
	            required
	          />
	        </div>

	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="password" className="form-label">
	            Password
	          </label>
	          <input
	            type="password"
	            className="form-control"
	            id="password"
	            name="password"
	            
	            value={formData.password}
	            onChange={handleChange}
	            required
	          />
	        </div>
	        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
	          <label htmlFor="confirmPassword" className="form-label">
	            Confirm Password
	          </label>
	          <input
	            type="password"
	            className="form-control"
	            id="confirmPassword"
	            name="confirmPassword"

	            value={confirmPassword}
	            onChange={handleConfirmPasswordChange}
	            required
	          />
	        </div>
	      <div className='text-center m-3'>
	      {
				isActive ? 
				<Button variant="primary" type="submit" className="btn btn-primary">Register</Button>
				: 
				<Button variant="danger" disabled>Register</Button>
				}
				</div>


	      </form>
	    </div>
  );
}


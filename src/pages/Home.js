import React from 'react';
import {Container, Col, Row, Carousel} from 'react-bootstrap'
import FeaturedProducts from '../components/FeaturedProducts';
import Banner from '../components/Banner';

export default function Home() {
  const bannerData = {
    title: "Out of the Oven",
    content: "Where Sweet Dreams Come Out of the Oven!",
    destination: "/menu",
    label: "Shop now!",
    textColor: '#faf0e6'
  };


  return (
    <>
    <div id='landing'>
    	<Container fluid>
	    	<Row>
	    		<Col className="d-flex align-items-center justify-content-center" style={{ height: '65vh' }}>
					 <Banner bannerData={bannerData} />
				  </Col>
			 </Row>
		  </Container>
    </div>

    <div id='home-featured' className='p-3'>
      <Container fluid>
        <Row>
          <Col className='col-12 text-center m-3'>
            <h2>Featured Products</h2>
          </Col>
          <Col>
            <FeaturedProducts/>
          </Col>
        </Row>
      </Container>
    </div>

    <div id='foryourinfo' className='p-3'>
      <Container>
        <Row>
          <Col className='text-center pt-3'>
          <h2>For your info!</h2>
          </Col>
        </Row>
        <Row>
          <Col id="datelabel" className='col-12 p-4 col-lg-4 text-center'>
            <h3>Date Label</h3>
            <div className='carousel-container'>
              <Carousel indicators={false}>

                <Carousel.Item>
                  <img src='https://i.ibb.co/CB0BVBQ/datelabel1.jpg' text="First slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                </Carousel.Item>

                <Carousel.Item>
                  <img src='https://i.ibb.co/3m9QzY7/datelabel2.jpg' text="Second slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                </Carousel.Item>

                <Carousel.Item>
                  <img src='https://i.ibb.co/31KCj6L/datelabel3.jpg' text="Third slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                </Carousel.Item>

              </Carousel>
            </div>
          </Col>
          <Col id="allergen" className='col-12 p-4 col-lg-4 text-center'>
            <h3>Allergen Information</h3>
              <div className='carousel-container'>
                <Carousel indicators={false}>

                  <Carousel.Item>
                    <img src='https://i.ibb.co/GJk7pr0/allergens1.jpg' text="First slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                  </Carousel.Item>

                  <Carousel.Item>
                    <img src='https://i.ibb.co/c2jqCPw/allergens2.jpg' text="Second slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                  </Carousel.Item>

                  <Carousel.Item>
                    <img src='https://i.ibb.co/9pNDx03/allergens3.jpg' text="Third slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                  </Carousel.Item>

                  <Carousel.Item>
                    <img src='https://i.ibb.co/PrTkd9s/allergens4.jpg' text="Fourth slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                  </Carousel.Item>

                  <Carousel.Item>
                    <img src='https://i.ibb.co/D1b5GCR/allergens5.jpg' text="Fifth slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                  </Carousel.Item>
                
                </Carousel>
              </div>
          </Col>
          <Col id="bestpair" className='col-12 p-4 col-lg-4 text-center'>
            <h2>Best Pairs</h2>
              <div className='carousel-container'>
                <Carousel indicators={false}>

                  <Carousel.Item>
                    <img src='https://i.ibb.co/WVY8gy7/bestpairs1.jpg' text="First slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                  </Carousel.Item>

                  <Carousel.Item>
                    <img src='https://i.ibb.co/zmRWNjN/bestpairs2.jpg' text="Second slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                  </Carousel.Item>

                  <Carousel.Item>
                    <img src='https://i.ibb.co/drMrZG4/bestpairs3.jpg' text="Third slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                  </Carousel.Item>

                  <Carousel.Item>
                    <img src='https://i.ibb.co/McD1DW1/bestpairs4.jpg' text="Fourth slide" style={{ maxWidth: '100%', maxHeight: '100%' }} />
                  </Carousel.Item>
                
                </Carousel>
              </div>
          </Col>
        </Row>
      </Container>
    </div>

    </>

  );
}

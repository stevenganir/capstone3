import { useContext, useEffect, useState } from 'react';
import { Navigate } from 'react-router-dom'; 
import UserContext from '../UserContext';
import CartView from '../components/CartView';

export default function Cart() {
  const { user } = useContext(UserContext);
  const [productsData, setProductsData] = useState([]);
  const [cart, setCart] = useState(null); // Initialize cart as null

  const fetchCart = async () => {
    try {
      const response = await fetch('https://capstone2-ganir.onrender.com/cart/user-cart', {
        headers: {
          Authorization: `Bearer ${user.token}`,
        }
      });

      if (response.ok) {
        const data = await response.json();
        setCart(data);
      } else {
        throw new Error('Failed to fetch cart data');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const fetchProductsData = async () => {
    try {
      const response = await fetch('https://capstone2-ganir.onrender.com/product/');

      if (response.ok) {
        const data = await response.json();
        setProductsData(data);
      } else {
        throw new Error('Failed to fetch product data');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  useEffect(() => {
    fetchProductsData();
    fetchCart();
  }, []); // Combine both fetch calls into one useEffect

  return (
    <>
      {user.token === null ? (
        <Navigate to="/login" />
      ) : cart === null ? (
        <h1>Loading...</h1> // Display "Loading..." while fetching cart data
      ) : (
        <CartView cartData={cart} productsData={productsData} fetchProductsData={fetchProductsData} fetchCart={fetchCart} />
      )}
    </>
  );
}
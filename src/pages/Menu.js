import { useEffect, useState, useContext } from 'react';

import AdminView from '../components/AdminView'
import UserView from '../components/UserView'
import UserContext from '../UserContext';

export default function Menu(){

	const {user} = useContext(UserContext)

	const [products, setProducts] = useState([])

	const [cart, setCart] = useState([])

	const fetchData = async () => {
		try {
		 	const response = await fetch('https://capstone2-ganir.onrender.com/product/');

		    if (response.ok) {
		      const data = await response.json();

		      if (data) {
		        setProducts(data);
		      }

		  	} 
		  }
		  	catch (error) {
		    	console.error('Error:', error);
		  	}
	};

	useEffect(() => {
		fetchData()
		fetchCart()
	}, [])

	const fetchCart = async () => {
    try {
      const response = await fetch('https://capstone2-ganir.onrender.com/cart/user-cart', {
        headers: {
          Authorization: `Bearer ${user.token}`,
        }
      });

      if (response.ok) {
        const data = await response.json();
        setCart(data);
      } else {
        throw new Error('Failed to fetch cart data');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  	};

  	useEffect(() => {
		fetchData()
		fetchCart()
	}, [])

	return(
		<>
		{
			(user.isAdmin === true)
			?
			<AdminView productsData={products} fetchData={fetchData} />
			:
			<UserView productsData={products} fetchData={fetchData} cartData={cart}  />
		}
		</>
	)
}
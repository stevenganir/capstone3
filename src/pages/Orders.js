import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom'; 
import AdminViewOrders from '../components/AdminViewOrders'
import UserViewOrders from '../components/UserViewOrders'


import UserContext from '../UserContext';

export default function Orders(){

	const {user} = useContext(UserContext)

	const [orders, setOrders] = useState([])

	const fetchOrders = async () => {
		try {
		 	const response = await fetch('https://capstone2-ganir.onrender.com/order/');

		    if (response.ok) {
		      const data = await response.json();

		      if (data) {
		        setOrders(data);
		      }

		  	} 
		  }
		  	catch (error) {
		    	console.error('Error:', error);
		  	}
	};

	

	useEffect(() => {
		fetchOrders()
	}, [])

	return(
		(user.token===null)?
	    <Navigate to="/menu" />
	    :

			(user.isAdmin)?
				<AdminViewOrders orders={orders} fetchOrders={fetchOrders} />
			:
				<UserViewOrders orders={orders} setOrders={setOrders}/>

	)
}
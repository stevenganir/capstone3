import React, { useState, useEffect, useContext } from 'react';
import { Button, Container, Col, Row } from 'react-bootstrap'
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);
  const { user, setUser } = useContext(UserContext);
  let token = ''

  const handleLogin = async (e) => {
  e.preventDefault();

  try {
    const response = await fetch('https://capstone2-ganir.onrender.com/user/authentication', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email, password }),
    });

    if (response.ok) {
      const data = await response.json();

      // Now you can use the token immediately
      token = data.accessToken;

      setEmail('');
      setPassword('');

      retrieveUserDetails(); // Pass the token as an argument



      Swal.fire({
        title: `${data.message}`,
        icon: 'success',
        text: `Welcome!`
      });
    } else {
      const data = await response.json();
      Swal.fire({
        title: `Login Failed`,
        icon: 'error',
        text: `${data.message}`
      });
    }
  } catch (error) {
    console.error('Error:', error);
  }
};

 const retrieveUserDetails = async () => {
  try {
    const response = await fetch('https://capstone2-ganir.onrender.com/user/user-details', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (response.ok) {
      const userData = await response.json();
      if (userData) {
        setUser({
          token: token,
          id: userData._id,
          isAdmin: userData.isAdmin,
          email: userData.email,
          firstName: userData.firstName,
          lastName: userData.lastName,
          password: userData.password,
          address: userData.address,
          cart: userData.cart
        });
      } else {
        console.error('Empty response received');
      }
    } else {
      // Handle error if user profile retrieval fails
      console.error('Failed to retrieve user profile');
    }
  } catch (error) {
    console.error('Error:', error);
  }
};

  useEffect(() => {


        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

  }, [email, password]);

  return (
    (user.token !== null) ?
        <Navigate to="/" />
    :
    <div id="login">
    <div className="container p-5">
      <h2 className='text-center'>Login</h2>
      <form onSubmit={handleLogin}>
        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
          <label htmlFor="email" className="form-label">
            Email
          </label>
          <input
            type="email"
            className="form-control"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        <div style={{ maxWidth: '500px', margin: '0 auto' }}>
          <label htmlFor="password" className="form-label">
            Password
          </label>
          <input
            type="password"
            className="form-control"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
        <div className='text-center mt-3'>
        { isActive ? 
                  <Button variant="primary" type="submit" id="submitBtn">
                      Login
                  </Button>
                  : 
                  <Button variant="danger" type="submit" id="submitBtn" disabled>
                      Login
                  </Button>
              }
        </div>
      </form>
      <Row className='text-center mt-2'>
        <Col>
          <p>
            Don't have an account?&nbsp;
            <Link to='/register'>Sign up</Link>
          </p>
        </Col>
      </Row>

    </div>
    </div>
  );
};

export default Login;
import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {

	// Consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider
    const { user, setUser } = useContext(UserContext);

    // Clear the localStorage of the user's information


    // Placing the "setUser" setter function inside of a useEffect is necessary because of updates within React JS that a state of another component cannot be updated while trying to render a different component
    // By adding the useEffect, this will allow the Logout page to render first before triggering the useEffect which changes the state of our user 
 
    const unsetUser = () => setUser({
        token: null,
        id: null,
        isAdmin: null,
        email: null,
        firstName: null,
        lastName: null,
        password: null,
        address: null,
        cart: null
    })

    useEffect(() => {
    // This effect runs once when the component mounts
    unsetUser();
  }, [unsetUser]);

    // Navigate back to login
    return (
        <Navigate to='/login' />
    )

}
import React, { useState, useContext, useEffect } from 'react';
import { Form, Button, Col, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function CreateProduct() {
  const [productData, setProductData] = useState({
    name: '',
    description: '',
    price: '',
    stockQuantity: '',
    imageUrl: 'https://i.ibb.co/GMVtkGx/placeholder.png',
  });

  const { user } = useContext(UserContext);

  const [isCreated, setIsCreated] = useState(false);

  const [isActive, setIsActive] = useState(false);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setProductData({
      ...productData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('https://capstone2-ganir.onrender.com/product/create', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify(productData),
      });

      const data = await response.json();

      if (response.status === 201) {
        // Product added successfully
        // You can perform any additional actions here

        Swal.fire({
          text: `${data.message}`,
          icon: 'success',
        });

        setProductData({
          name: '',
          description: '',
          price: '',
          stockQuantity: '',
          imageUrl: '',
        });

        setIsCreated(true);
      } else {
        // Handle errors or show appropriate messages
        Swal.fire({
          text: `${data.message}`,
          icon: 'error',
        });
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  useEffect(() => {
    if (
      productData.name !== '' &&
      productData.description !== '' &&
      productData.price !== ''
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productData.name, productData.description, productData.price]);

  return (
    <>
      {user.token === null ? (
        <Navigate to="/menu" />
      ) : isCreated === true ? (
        <Navigate to="/menu" />
      ) : (
        <div id='createproduct'>
        <Container fluid className="p-3 text-center">
        <h1 className="text-center my-4" style={{color: 'white', fontSize: '4rem', fontWeight:'bold'}}>Create a Product</h1>
          <div style={{ maxWidth: '1000px', margin: '0 auto' }}>
            <Form onSubmit={handleSubmit} className="m-1">
              <Form.Group controlId="productName" className="m-3">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  value={productData.name}
                  onChange={handleInputChange}
                  required
                  className='text-center'
                />
              </Form.Group>

              <Form.Group controlId="productDescription" className="m-3">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  name="description"
                  value={productData.description}
                  onChange={handleInputChange}
                  required
                  className='text-center'
                />
              </Form.Group>

              <Form.Group controlId="productPrice" className="m-3">
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  name="price"
                  value={productData.price}
                  onChange={handleInputChange}
                  required
                  className='text-center'
                />
              </Form.Group>

              <Form.Group controlId="productStockQuantity" className="m-3">
                <Form.Label>Stock Quantity</Form.Label>
                <Form.Control
                  type="number"
                  name="stockQuantity"
                  placeholder="0"
                  value={productData.stockQuantity}
                  onChange={handleInputChange}
                  className='text-center'
                />
              </Form.Group>

              <Form.Group controlId="productStockQuantity" className="m-3">
                <Form.Label>Image URL</Form.Label>
                <Form.Control
                  type="text"
                  name="imageUrl"
                  placeholder=""
                  value={productData.imageUrl}
                  onChange={handleInputChange}
                  className='text-center'
                />
              </Form.Group>

              <Form.Group as={Col} controlId="productImagePreview" className="m-3">
                <Form.Label>Image Preview:</Form.Label>
                <br/>
                <img style={{ width: '30%' }} src={productData.imageUrl} alt="Product" />
              </Form.Group>

              <div className="d-flex justify-content-center m-3">
                {isActive ? (
                  <Button variant="success" type="submit" className="mt-2">
                    Create Product
                  </Button>
                ) : (
                  <Button variant="danger" disabled className="mt-2">
                    Create Product
                  </Button>
                )}
              </div>
            </Form>
          </div>
        </Container>
        </div>
      )}
    </>
  );
}

import { useContext, useEffect, useState } from 'react'
import { Navigate } from 'react-router-dom'
import { Container, Col, Row, Button, Modal, Table, Form } from 'react-bootstrap'
import Swal from 'sweetalert2'

import ArchiveVoucher from '../components/ArchiveVoucher'

import UserContext from '../UserContext'

export default function Vouchers() {
  const { user } = useContext(UserContext)
  const [vouchersData, setVouchersData] = useState([])
  const [vouchers, setVouchers] = useState([])
  const [selectedVoucher, setSelectedVoucher] = useState(null)
  const [showDescriptionModal, setShowDescriptionModal] = useState(false)
  const [newVoucherData, setNewVoucherData] = useState({
    name: '',
    code: '',
    type: 'amount', // Default type, you can change it as needed
    value: '', // Default value, you can change it as needed
  });

  const fetchAllVouchers = async () => {
    fetch('https://capstone2-ganir.onrender.com/voucher/', {
      headers: {
        Authorization: `Bearer ${user.token}`
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setVouchersData(data)
      })
  }

  const handleShowModal = () => {
  	setShowDescriptionModal(true);
  };

  const handleCloseModal = () => {
  	setShowDescriptionModal(false);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewVoucherData({
      ...newVoucherData,
      [name]: value,
    });
  };

  const handleCreateVoucher = async () => {
	  try {
	    const response = await fetch('https://capstone2-ganir.onrender.com/voucher/create-voucher', {
	      method: 'POST',
	      headers: {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${user.token}`
	      },
	      body: JSON.stringify(newVoucherData)
	    });

	    const data = await response.json();

	    if (response.ok) {
	    	Swal.fire({
          		text: `${data.message}`,
          		icon: 'success',
        	});
	    }else{
	    	Swal.fire({
          		text: `${data.message}`,
          		icon: 'error',
        	});
	    }

	    handleCloseModal();
	    fetchAllVouchers();
	  } catch (error) {
	    // Handle errors here, e.g., show an error message to the user
	    console.error('Error creating voucher:', error);
	  }
	}
  

  console.log(vouchersData)

  useEffect(() => {
    fetchAllVouchers()
  }, [])

  useEffect(() => {
    const vouchersArr = vouchersData.map((voucher, index) => {
      return (
        <tr key={voucher._id}>
        	<td>{index + 1}</td>
          	<td>{voucher._id}</td>
          	<td>{voucher.name}</td>
          	<td>{voucher.code}</td>
          	<td>{voucher.type}</td>
          	
          	{(voucher.type === 'amount') ?
          		<td>₱{voucher.value}</td>
          		:
          		<td>{voucher.value}%</td>
          	}
          	
          	<td className={voucher.isActive ? 'text-success' : 'text-danger'}>
            	{voucher.isActive ? 'Available' : 'Unavailable'}
          	</td>
          	<td>
            	<ArchiveVoucher
              		voucher={voucher._id}
              		isActive={voucher.isActive}
              		fetchAllVouchers={fetchAllVouchers}
            	/>
            </td>
        </tr>
      );
    });

    setVouchers(vouchersArr);
  }, [vouchersData]);

  
  return (
    (user.token === null) ? (
      <Navigate to="/login" />
    ) : (
      <>
        {vouchersData.length === 0 ? ( // Check if usersData is empty
          <p>Loading...</p>
        ) : (
          user.isAdmin ? (
            <>
              <div id='vouchers'>
              <Container className='p-3'>
              	<Row className='text-center'>
              		<Col className='col-4'>
              		</Col>
              		<Col className='col-4'>
              			<h1>All Vouchers</h1>
              		</Col>
              		<Col className='col-4 d-flex justify-content-center align-items-center'>
              			<Button className='mb-3' variant='success' onClick={handleShowModal}>
            				Create New Voucher
          				</Button>
              		</Col>
              	</Row>
                <Row>
                  <Col>
                    <Table striped bordered hover responsive className="mt-4">
                      <thead>
                        <tr className="text-center">
                        	<td>#</td>
                         	<th>Voucher ID</th>
                         	<th>Name</th>
                         	<th>Code</th>
                         	<th>Type</th>
                          	<th>Value</th>
                          	<th>Availability</th>
                          	<th colSpan="2">Actions</th>
                        </tr>
                      </thead>
                      <tbody className="text-center">{vouchers}</tbody>
                    </Table>
                  </Col>
                </Row>
              </Container>
              </div>

              <Modal show={showDescriptionModal} onHide={handleCloseModal}>
			  	<Modal.Header closeButton>
			    	<Modal.Title>Create New Voucher</Modal.Title>
			    </Modal.Header>
			    <Modal.Body>
			    	<Form>
			        	<Form.Group>
			            	<Form.Label>Voucher Name</Form.Label>
			            	<Form.Control
			              		type='text'
			              		name='name'
			              		value={newVoucherData.name}
			              		onChange={handleInputChange}
			            	/>
			          	</Form.Group>

			          	<Form.Group>
			            	<Form.Label>Code</Form.Label>
			            	<Form.Control
			              		type='text'
			              		name='code'
			              		value={newVoucherData.code}
			              		onChange={handleInputChange}
			            	/>
			          	</Form.Group>

			          	<Form.Group>
			            	<Form.Label>Type</Form.Label>
			            	<Form.Control
			              	as='select'
			              		name='type'
			              		value={newVoucherData.type}
			              		onChange={handleInputChange}
			            	>
			              	<option value='amount'>Amount</option>
			              	<option value='percent'>Percent</option>
			            	</Form.Control>
			          	</Form.Group>

			          	<Form.Group>
			            	<Form.Label>Value</Form.Label>
			            	<Form.Control
			              		type='number'
			              		name='value'
			              		value={newVoucherData.value}
			              		onChange={handleInputChange}
			            	/>
			          	</Form.Group>
			        </Form>
			   	</Modal.Body>
			    <Modal.Footer>
			    	<Button variant='secondary' onClick={handleCloseModal}>
			        	Cancel
			        </Button>
			        <Button variant='success' onClick={handleCreateVoucher}>
			        	Create
			        </Button>
			     </Modal.Footer>
			    </Modal>


            </>
          ) : (
            <Navigate to="/home" />
          )
        )}
      </>
    )
  );
}

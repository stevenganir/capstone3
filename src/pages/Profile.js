import { useContext, useEffect, useState } from 'react'
import { Container, Row, Col, Button, Modal } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'

import UpdateProfile from '../components/UpdateProfile'
import UpdateAddress from '../components/UpdateAddress'
import UserContext from '../UserContext'

export default function Profile() {
  const { user } = useContext(UserContext)
  const [details, setDetails] = useState(null) // Initialize details as null

 const [showEditProfileModal, setShowEditProfileModal] = useState(false);
 const [showEditAddressModal, setShowEditAddressModal] = useState(false);

  const handleEditProfile = () => {
    setShowEditProfileModal(true);
  };

  const handleCloseEditProfileModal = () => {
    setShowEditProfileModal(false);
  };

   const handleEditAddress = () => {
    setShowEditAddressModal(true);
  };

  const handleCloseEditAddressModal = () => {
    setShowEditAddressModal(false);
  };

  const fetchUserDetails = () =>{
  	fetch('https://capstone2-ganir.onrender.com/user/user-details', {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data)
        setDetails(data)
      })
  }

  useEffect(() => {
    fetchUserDetails()
  }, [])

  return (
    (user.token === null) ? (
      <Navigate to='/login' />
    ) : (
      <>
        {details ? (
          <div id='profile'>
          <Container fluid>
            <Row className='p-3'>
              <Col className='text-center'>
                <h1>What's up, {`${details.firstName}`}?</h1>
              </Col>
            </Row>

            <Row>
              <Col className='col-lg-6 p-3 col-12'>
                <Row>
                  <Col className='text-center'>
                    <h4>Profile Details:</h4>
                  </Col>
                </Row>
                <Row className='text-center'>
                	<Col className='col-12'>
                		<p>Email: {`${details.email}`}</p>
                	</Col>
                	<Col className='col-12'>
                		<p>First Name: {`${details.firstName}`}</p>
                	</Col>
                	<Col className='col-12'>
                		<p>Last Name: {`${details.lastName}`}</p>
                	</Col>
                	<Col className='col-12'>
                		<p>Password: ••••••••</p>
                	</Col>
                  <Col className='col-12'>
                    <Button onClick={handleEditProfile}>Edit Profile</Button>
                  </Col>
                </Row>
              </Col>

              <Col className='col-lg-6 p-3 col-12'>
                <Row>
                	<Col className='text-center'>
                		<h4>Address Details:</h4>
                	</Col>
                </Row>
                <Row className='text-center'>
                	<Col className='col-12'>
                		<p>Region: {`${details.address.region}`}</p>
                	</Col>
                	<Col className='col-12'>
                		<p>Province: {`${details.address.province}`}</p>
                	</Col>
                	<Col className='col-12'>
                		<p>City: {`${details.address.city}`}</p>
                	</Col>
                	<Col className='col-12'>
                		<p>Barangay: {`${details.address.barangay}`}</p>
                	</Col>
                	<Col className='col-12'>
                		<p>Street Name: {`${details.address.streetName}`}</p>
                	</Col>
                	<Col className='col-12'>
                		<p>Building/House Number: {`${details.address.buildingHouseNumber}`}</p>
                	</Col>
                  <Col className='col-12'>
                    <Button onClick={handleEditAddress}>Edit Address</Button>
                  </Col>
                
                </Row>
                </Col>

            </Row>
          </Container>
          </div>
        ) : (
          <p className='text-center'>Loading...</p>
        )}
        <Modal show={showEditProfileModal} onHide={handleCloseEditProfileModal}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <UpdateProfile
            details={details}
            fetchUserDetails={fetchUserDetails}
            closeModal={handleCloseEditProfileModal}
          />
        </Modal.Body>
      </Modal>

      <Modal show={showEditAddressModal} onHide={handleCloseEditAddressModal}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Address</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <UpdateAddress
            details={details}
            fetchUserDetails={fetchUserDetails}
            closeModal={handleCloseEditAddressModal}
          />
        </Modal.Body>
      </Modal>
      
      </>
    )
  )
}

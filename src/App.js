
import { useState, useEffect } from 'react'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap'

import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Menu from './pages/Menu'
import Orders from './pages/Orders'
import CreateProduct from './pages/CreateProduct'
import Cart from './pages/Cart'
import Error from './pages/Error';
import Profile from './pages/Profile'
import Users from './pages/Users'
import Vouchers from './pages/Vouchers'
import { UserProvider } from './UserContext';



import './App.css';

function App() {
  const [user, setUser] = useState({
    token: null,
    id: null,
    isAdmin: null,
    email: null,
    firstName: null,
    lastName: null,
    password: null,
    address: null,
    cart: null
  });



  return (
    <UserProvider value={{user, setUser}}>
      <Router>
        <Container fluid className="p-0">

          <AppNavbar />

           <Routes>

              <Route path="/" element={<Home/>} />
              <Route path="/register" element={<Register/>} />
              <Route path="/login" element={<Login/>} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/menu" element={<Menu />} />
              <Route path="/createproduct" element={<CreateProduct />} />
              <Route path="/orders" element={<Orders />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="*" element={<Error />} />
              <Route path="/profile" element={<Profile />} />
              <Route path="/users" element={<Users />} />
              <Route path="/vouchers" element={<Vouchers />} />

          </Routes>

        </Container>
      </Router> 
    </UserProvider>
  );

}

export default App;

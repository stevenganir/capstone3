import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({bannerData}) {

    const {title, content, destination, label, textColor} = bannerData;

    return (
        <Row>
            <Col className="p-5 text-center">
                <h1 style={{ color: `${textColor}`, fontSize: '3em', fontWeight:'bold' }}>{title}</h1>
                <p style={{ color: `${textColor}`, fontWeight: 'bold', fontSize: '1.5em' }}>{content}</p>
                <Link className="btn btn-primary" to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}
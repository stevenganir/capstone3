import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useContext } from 'react'

import UserContext from '../UserContext';

export default function ArchiveProduct({product, isActive, fetchData}) {

	const { user, setUser } = useContext(UserContext);

	//console.log(user.token)

	const archiveToggle = async (productId) => {
	  try {
	    const response = await fetch(`https://capstone2-ganir.onrender.com/product/archive-product/${productId}`, {
	      method: 'PUT',
	      headers: {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${user.token}`
	      }
	    });

	    const data = await response.json();

	    if (response.ok) {
	      Swal.fire({
	        text: `${data.message}`,
	        icon: 'success',
	      });
	    } else {
	      Swal.fire({
	        text: `${data.message}`,
	        icon: 'error'
	      });
	    }

	    fetchData();
	  } catch (error) {
	    console.error('Error:', error);
	    fetchData();
	  }
	}



	const activateToggle = async (productId) => {
	  try {
	    const response = await fetch(`https://capstone2-ganir.onrender.com/product/activate-product/${productId}`, {
	      method: 'PUT',
	      headers: {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${user.token}`
	      }
	    });

	    const data = await response.json();

	    if (response.ok) {
	      Swal.fire({
	        text: `${data.message}`,
	        icon: 'success',
	      });
	    } else {
	      Swal.fire({
	        text: `${data.message}`,
	        icon: 'error'
	      });
	    }

	    fetchData();
	  } catch (error) {
	    console.error('Error:', error);
	    fetchData();
	  }
	}
 

	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => activateToggle(product)}>Activate</Button>

			}
		</>

		)
}
import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import { Col, Row, Container } from 'react-bootstrap'

import SearchProducts from './SearchProducts'
import Checkout from './Checkout'

export default function UserView({ productsData, fetchData, cartData }) {
  const [products, setProducts] = useState([]);

  const [searchResults, setSearchResults] = useState([]);

  const [checkoutArr, setCheckoutArr] = useState([])

  let activeProducts;

  useEffect(() => {
    if(searchResults.length ===0){
      activeProducts = productsData.filter((product) => product.isActive === true);
    }else{
      activeProducts = searchResults.filter((product) => product.isActive === true);
    }



    const productsArr = [];
    for (let i = 0; i < activeProducts.length; i += 4) {
      const productRow = activeProducts.slice(i, i + 4).map((product, index) => (
        <Col
          key={product._id}
          xs={12}  // For mobile screens, occupy all 12 columns
          sm={6}   // For tablets, occupy 6 columns
          lg={3}   // For large screens, occupy 3 columns
          className={`text-center mb-3 ${
            index % 2 === 0 ? 'order-lg-1' : 'order-lg-2'
          }`}
        >
          <ProductCard productsData={productsData} product={product} setCheckoutArr={setCheckoutArr} />
        </Col>
      ));
      productsArr.push(
        <Row key={`row-${i}`} className="justify-content-center align-items-center">
          {productRow}
        </Row>
      );
    }

    setProducts(productsArr);
    fetchData()
  }, [productsData, searchResults]);



  return (
    <>
      { (checkoutArr.length ===0) 
        ?
        	<>
          <div id='userview'>
          <Container>
            <h1 className="text-center p-4" style={{color: 'white', fontSize: '4rem', fontWeight:'bold'}}>Out of the Oven Products</h1>
            <SearchProducts setSearchResults={setSearchResults}/>
          </Container>
          <Container>
            {products}
          </Container>
          </div>
          </>
        :
          <>
          <Checkout cartData={cartData} checkoutArr={checkoutArr} setCheckoutArr={setCheckoutArr} productsData={productsData}/>
          </>
      }
    </>
  );
}
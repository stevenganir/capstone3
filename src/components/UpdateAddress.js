import React, { useState, useEffect, useContext } from 'react';
import { Button, Form, Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function UpdateAddress({ details, fetchUserDetails, closeModal }) {
	const { user } = useContext(UserContext)
  const [formData, setFormData] = useState({
    region: details.address.region,
    province: details.address.province,
    city: details.address.city,
    barangay: details.address.barangay,
    streetName: details.address.streetName,
    buildingHouseNumber: details.address.buildingHouseNumber,
  });

  const isAnyFieldEmpty = () => {
  for (const key in formData) {
    if (formData[key].trim() === '') {
      return true;
    }
  }
  return false;
};

  useEffect(() => {
    // Update formData when the details prop changes
    setFormData({
      region: details.address.region,
      province: details.address.province,
      city: details.address.city,
      barangay: details.address.barangay,
      streetName: details.address.streetName,
      buildingHouseNumber: details.address.buildingHouseNumber,
    });
  }, [details]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
  e.preventDefault();

  try {
    // Send a request to update the user's profile with formData
    const response = await fetch('https://capstone2-ganir.onrender.com/user/update-user-details', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`,
      },
      body: JSON.stringify(formData),
    });

    const data = await response.json();
    if (!response.ok) {
      Swal.fire({
      text: `${data.message}`,
      icon: 'error',
      });
    }

    
    if(response.ok){
    console.log(data);
    await fetchUserDetails(); // Fetch updated user details
    closeModal(); // Close the modal
    Swal.fire({
      text: `${data.message}`,
      icon: 'success',
    });
    }
  } catch (error) {
    console.error('Error updating profile:', error);
  }
};

  return (
    <Form onSubmit={handleSubmit}>
      <Container>
        <Form.Group controlId="region">
          <Form.Label>Region</Form.Label>
          <Form.Control
            type="text"
            name="region"
            value={formData.region}
            onChange={handleChange}
          />
        </Form.Group>

        <Form.Group controlId="province">
          <Form.Label>Province</Form.Label>
          <Form.Control
            type="text"
            name="province"
            value={formData.province}
            onChange={handleChange}
          />
        </Form.Group>

        <Form.Group controlId="city">
          <Form.Label>City</Form.Label>
          <Form.Control
            type="text"
            name="city"
            value={formData.city}
            onChange={handleChange}
          />
        </Form.Group>

        <Form.Group controlId="barangay">
          <Form.Label>Barangay</Form.Label>
          <Form.Control
            type="text"
            name="barangay"
            value={formData.barangay}
            onChange={handleChange}
          />
        </Form.Group>

        <Form.Group controlId="streetName">
          <Form.Label>Street Name</Form.Label>
          <Form.Control
            type="text"
            name="streetName"
            value={formData.streetName}
            onChange={handleChange}
          />
        </Form.Group>

        <Form.Group controlId="buildingHouseNumber">
          <Form.Label>Building/House Number</Form.Label>
          <Form.Control
            type="text"
            name="buildingHouseNumber"
            value={formData.buildingHouseNumber}
            onChange={handleChange}
          />
        </Form.Group>

        <Row className="text-center mt-3">
          <Col>
            <Button variant="primary" type="submit" disabled={isAnyFieldEmpty()}>
              Save Changes
            </Button>
          </Col>
        </Row>
      </Container>
    </Form>
  );
}

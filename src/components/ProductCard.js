import { useState, useContext, useEffect } from 'react';
import { Card, Button, Modal, Container, Col, Row } from 'react-bootstrap';
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom';
import { Navigate, useNavigate } from 'react-router-dom';

import UserContext from '../UserContext'


export default function ProductCard({product, setCheckoutArr, productsData, fetchData}) {

	const navigate = useNavigate();

	const {user, setUser} = useContext(UserContext)

	const { _id} = product;

	const [show, setShow] = useState(false);

	const matchedProduct = productsData.find((dataProduct) => dataProduct._id === _id);

	 const { name, description, price, stockQuantity, imageUrl } = matchedProduct;


	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);

  	 const handleBuy = () => {

  	 	if (user.token !== null){
    
		    setCheckoutArr([
		      {
		        productName: name,
		        quantity: 1,
		      },
		    ]);
		}else{
			navigate("/login")
		}

	  };

	  const handleAddToCart = async () => {

	  	if (user.token !== null){
    
		    try {
		      const response = await fetch('https://capstone2-ganir.onrender.com/cart/addToCart', {
		        method: 'POST',
		        headers: {
		          'Content-Type': 'application/json',
		          Authorization: `Bearer ${user.token}`,
		        },
		        body: JSON.stringify({
		          productName: name,
		          quantity: 1
		        }),
		      });

		      const data = await response.json();

		      if (response.status === 200) {
		        Swal.fire({
		          text: `${data.message}`,
		          icon: 'success',
		        });
		      } else {
		        Swal.fire({
		          title: `${data.message}`,
		          icon: 'error',
		        });
		      }

		      navigate('/menu');
		    } catch (error) {
		      console.error('Error creating order');
		      // You can display an error message to the user or handle the error as needed
		    }


		}else{
			navigate("/login")
		}

	  };

	 


	return (
		<>
		<Card className="mt-3" style={{ width: '18rem'}}>
			<Button variant="light" onClick={()=>handleShow()}>
				<Card.Img variant="top" style={{ maxHeight: '200px' }} src={imageUrl} alt={`${name}`} />
			</Button>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>₱{price}</Card.Text>
		        {/*<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>*/}
		        <Button variant="success" className="mt-2" onClick={handleBuy}>Buy Now</Button>
		        <br/>
                <Button variant="warning" className="mt-2" onClick={handleAddToCart}>Add to Cart</Button>
            </Card.Body>
        
        </Card>

        <Modal size="lg" show={show} onHide={handleClose} aria-labelledby="example-modal-sizes-title-lg">
  			<Modal.Header closeButton></Modal.Header>
  			<Modal.Body className='text-center'>
   	 			<Container>
      				<Row>
        				<Col xs={12} md={6}>
         					<img style={{ width: '100%' }} src={imageUrl} alt={name} />
        				</Col>
        				<Col xs={12} md={6}>
          					<h1>{name}</h1>
          					<br/>
          					<h5>Description:</h5>
          					<p>{description}</p>
          					<br/>
          					<h5>Price:</h5>
          					<h4>₱{price}</h4>
          					<br/>
          					<h5>Available Stocks:</h5>
          					<h4>{stockQuantity}</h4>
        				</Col>
      				</Row>
    			</Container>
  			</Modal.Body>
  			<Modal.Footer>
    			<Button variant="success" className="mt-2" onClick={handleBuy}>Buy Now</Button>
    			<Button variant="warning" className="mt-2" onClick={handleAddToCart}>Add to Cart</Button>
  			</Modal.Footer>
		</Modal>
        </>
		)
}


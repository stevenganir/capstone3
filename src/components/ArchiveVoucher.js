import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useContext } from 'react'

import UserContext from '../UserContext';

export default function ArchiveProduct({voucher, isActive, fetchAllVouchers}) {

	const { user, setUser } = useContext(UserContext);

	//console.log(user.token)

	const archiveToggle = async (voucherId) => {
	  try {
	    const response = await fetch(`https://capstone2-ganir.onrender.com/voucher/archive-voucher/${voucherId}`, {
	      method: 'PUT',
	      headers: {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${user.token}`
	      }
	    });

	    const data = await response.json();

	    if (response.ok) {
	      Swal.fire({
	        text: `${data.message}`,
	        icon: 'success',
	      });
	    } else {
	      Swal.fire({
	        text: `${data.message}`,
	        icon: 'error'
	      });
	    }

	    fetchAllVouchers();
	  } catch (error) {
	    console.error('Error:', error);
	    fetchAllVouchers();
	  }
	}



	const activateToggle = async (voucherId) => {
	  try {
	    const response = await fetch(`https://capstone2-ganir.onrender.com/voucher/activate-voucher/${voucherId}`, {
	      method: 'PUT',
	      headers: {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${user.token}`
	      }
	    });

	    const data = await response.json();

	    if (response.ok) {
	      Swal.fire({
	        text: `${data.message}`,
	        icon: 'success',
	      });
	    } else {
	      Swal.fire({
	        text: `${data.message}`,
	        icon: 'error'
	      });
	    }

	    fetchAllVouchers();
	  } catch (error) {
	    console.error('Error:', error);
	    fetchAllVouchers();
	  }
	}
 

	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle(voucher)}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => activateToggle(voucher)}>Activate</Button>

			}
		</>

		)
}
import React, { useState, useContext } from 'react';
import { Button, Form, Row, Col, Container } from 'react-bootstrap';
import Swal from 'sweetalert2'

import UserContext from '../UserContext';

export default function UpdateProfile({ details, fetchUserDetails, closeModal }) {
  const { user } = useContext(UserContext);
  const [formData, setFormData] = useState({
    firstName: details.firstName,
    lastName: details.lastName,
    password: '', // Initialize the password field
  });

  const isAnyFieldEmpty = () => {
    for (const key in formData) {
      if (key !== 'password' && formData[key].trim() === '') {
        return true;
      }
    }
    return false;
  }

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
  e.preventDefault();

  try {
    // Send a request to update the user's profile with formData
    const response = await fetch('https://capstone2-ganir.onrender.com/user/update-user-details', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`,
      },
      body: JSON.stringify(formData),
    });

    const data = await response.json();
    if (!response.ok) {
      Swal.fire({
      text: `${data.message}`,
      icon: 'error',
    });
    }

    if(response.ok){
    console.log(data);
    await fetchUserDetails(); // Fetch updated user details
    closeModal(); // Close the modal
    Swal.fire({
      text: `${data.message}`,
      icon: 'success',
    });
    }
  } catch (error) {
    console.error('Error updating profile:', error);
  }
};

  return (
    <Form onSubmit={handleSubmit}>
      <Container>
        <Form.Group controlId="firstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            type="text"
            name="firstName"
            value={formData.firstName}
            onChange={handleChange}
          />
        </Form.Group>

        <Form.Group controlId="lastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
            type="text"
            name="lastName"
            value={formData.lastName}
            onChange={handleChange}
          />
        </Form.Group>

        <Form.Group controlId="password"> 
          <Form.Label>New Password</Form.Label>
          <Form.Control
            type="password" 
            name="password"
            value={formData.password}
            onChange={handleChange}
            autoComplete='off'
          />
        </Form.Group>

        <Row className="text-center mt-3">
          <Col>
            <Button variant="primary" type="submit"  disabled={isAnyFieldEmpty()} >
              Save Changes
            </Button>
          </Col>
        </Row>
      </Container>
    </Form>
  );
}

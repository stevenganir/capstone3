import { Col, Card, Row, Button, } from 'react-bootstrap'
import { Link, Navigate } from 'react-router-dom'

export default function Product(props){
	const { breakPoint, data } = props

	const { _id, name, description, price, imageUrl } = data


	return(
		<Col xs={12} md={breakPoint} style={{ width: '18rem'}}>
			<Card className="cardHighlight mx-2">
				<Link className="btn btn-light" to='/menu'>
				<Card.Img variant="top" style={{ maxHeight: '200px' }} src={imageUrl} alt={`${name}`} />
				</Link>
				<Card.Body>
					<Row className='dflex justify-content-center align-items-center'>
						<Col className='col-6 text-center'>
							<Card.Text>{name}</Card.Text>
						</Col>
						<Col className='col-6 text-center'>
							<Card.Text>₱{price}</Card.Text>
						</Col>
					</Row>
				</Card.Body>

			</Card>
		</Col>
	)
}

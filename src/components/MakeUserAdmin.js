import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useContext } from 'react'

import UserContext from '../UserContext';

export default function MakeUserAdmin({userId, isAdmin, fetchAllUsers}) {

	const { user, setUser } = useContext(UserContext);

	const makeAdmin = async (userId) => {
	  try {
	    const response = await fetch(`https://capstone2-ganir.onrender.com/user/make-admin`, {
	      method: 'PUT',
	      headers: {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${user.token}`
	      },
	      body: JSON.stringify({
            userId: userId
          })
	    });

	    const data = await response.json();

	    if (response.ok) {
	      Swal.fire({
	        text: `${data.message}`,
	        icon: 'success',
	      });
	    } else {
	      Swal.fire({
	        text: `${data.message}`,
	        icon: 'error'
	      });
	    }

	    fetchAllUsers();
	  } catch (error) {
	    console.error('Error:', error);
	    fetchAllUsers();
	  }
	}





	return(
		<>
			{isAdmin ?

				<Button variant="secondary" size="sm" disabled>Admin</Button>

				:

				<Button variant="success" size="sm" onClick={() => makeAdmin(userId)}>Make Admin</Button>
				

			}
		</>

		)
}
import React, { useState, useEffect, useContext } from 'react';
import { Card, Button, Container, Col, Row, Form, Image } from 'react-bootstrap'; // Import Image component
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

const centerButtonStyle = {
  display: 'flex',
  justifyContent: 'center',
  marginTop: '1rem',
};

export default function Checkout({ checkoutArr, setCheckoutArr, productsData, cartData }) {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const cart = cartData
  const [cartFetched, setCartFetched] = useState(false);
  const [address, setAddress] = useState({})
  const [modeOfPayment, setModeOfPayment] = useState('Cash on Delivery');
  const [voucher, setVoucher] = useState('')
  const [fetchedVoucher, setFetchedVoucher] = useState({})
  const [voucherCode, setVoucherCode] = useState('')
  const [isEqual, setIsEqual] = useState(false)
  const [totalAmount, setTotalAmount] = useState(0)

  

  // Define a function to handle quantity changes
  const handleQuantityChange = (index, newQuantity) => {
    // Update the quantity directly in the checkoutArr
    checkoutArr[index].quantity = newQuantity;
    // Trigger a re-render by setting the modified checkoutArr
    setCheckoutArr([...checkoutArr]);
  };

  // Define a function to increment quantity
  const handleIncrement = (index) => {
    const updatedQuantity = parseInt(checkoutArr[index].quantity, 10) + 1;
    handleQuantityChange(index, updatedQuantity);
  };

  // Define a function to decrement quantity
  const handleDecrement = (index) => {
    const updatedQuantity = Math.max(parseInt(checkoutArr[index].quantity, 10) - 1, 0);
    handleQuantityChange(index, updatedQuantity);
  };

  const handleCancel = () => {
    setCheckoutArr([]);
  };

  // Define a function to create an order
  const createOrder = async () => {
    try {
      const response = await fetch('https://capstone2-ganir.onrender.com/order/create-order', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify({
          products: checkoutArr,
          modeOfPayment: modeOfPayment,
          voucherCode: voucherCode
        }),
      });

      const data = await response.json();

      if (response.status === 201) {
        Swal.fire({
          text: `${data.message}`,
          icon: 'success',
        });
        setCheckoutArr([]);
        navigate('/menu');
      } else {
        Swal.fire({
          text: `${data.message}`,
          icon: 'error',
        });
        setVoucher('')
      }

      
    } catch (error) {
      console.error('Error creating order');
      // You can display an error message to the user or handle the error as needed
    }
  };

  const fetchUserDetails = () =>{
    fetch('https://capstone2-ganir.onrender.com/user/user-details', {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setAddress(data.address)
              })
  }

  const checkoutCart = async () => {
    try {
      const response = await fetch('https://capstone2-ganir.onrender.com/cart/checkout', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify({
          modeOfPayment: modeOfPayment,
          voucherCode: voucherCode
        }),
      });

      const data = await response.json();

      if (response.status === 201) {
        Swal.fire({
          text: `${data.message}`,
          icon: 'success',
        });
        setCheckoutArr([]);
        setIsEqual(false)
        navigate('/menu');
      } else {
        Swal.fire({
          text: `${data.message}`,
          icon: 'error',
        });
      }

    } catch (error) {
      console.error('Error creating order');
      // You can display an error message to the user or handle the error as needed
    }
  };

  const applyVoucher = async () => {

    try{
      const response = await fetch('https://capstone2-ganir.onrender.com/voucher/apply-voucher', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify({
          voucherCode: voucher
        })
      })

      const data = await response.json()

      if (response.ok) {
        Swal.fire({
          text: `${data.message}`,
          icon: 'success',
        });
        setFetchedVoucher(data)

        Swal.fire({
          text: `${data.name} applied!`,
          icon: 'success',
        });

      } else {
        Swal.fire({
          text: `${data.message}`,
          icon: 'error',
        });
        setVoucher('')
        }

    } catch(error){
      console.error('Error fetching voucher')
    }
  }

  const cardElements = checkoutArr.map((item, index) => {
    // Find the product in productsData based on productName
    const product = productsData.find((product) => product.name === item.productName);

    return (
      <Row key={index}>
        <Col>
          <Card>
            <Row>
              {/* Left Part (Product Image) */}
              <Col md={4}>
                <Image
                  src={product.imageUrl}
                  style={{ maxWidth: '50%', height: 'auto' }} className='mt-5' // Inline style for the image
                />
              </Col>
              {/* Right Part (Product Information and Quantity) */}
              <Col md={8}>
                <Card.Body>
                  <Card.Title>{product.name}</Card.Title>
                  <Card.Subtitle>Description:</Card.Subtitle>
                  <Card.Text>{product.description}</Card.Text>
                  <Card.Subtitle>Price:</Card.Subtitle>
                  <Card.Text>₱{product.price}</Card.Text>
                  <Card.Subtitle>Available Stocks:</Card.Subtitle>
                  <Card.Text>{product.stockQuantity}</Card.Text>
                  <Card.Subtitle>Quantity:</Card.Subtitle>
                  <Card.Text>
                    <Button className="mx-2" variant="dark" size="sm" onClick={() => handleDecrement(index)}>
                      -
                    </Button>
                    <input
                      className="text-center"
                      type="number"
                      value={item.quantity}
                      onChange={(e) => handleQuantityChange(index, e.target.value)}
                    />
                    <Button className="mx-2" variant="dark" size="sm" onClick={() => handleIncrement(index)}>
                      +
                    </Button>
                  </Card.Text>
                </Card.Body>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    );
  });

  const checkCartEquality = () => {
    if(cart !== null){
    const isEqual = checkoutArr.every((checkoutProduct) => {
      const matchingUserCartProduct = cart.cart.addedProducts.find((userCartProduct) => {
        return checkoutProduct.productName === userCartProduct.productName
      });
      return !!matchingUserCartProduct; // Check if a matching product was found in checkoutArr
    });

    setIsEqual(isEqual); // Update the state
    }
  };

  const handleNavigateToProfile = () => {
    navigate('/profile');
  };
  
  useEffect(() => {
    fetchUserDetails()
  }, [])

  useEffect(() => {
    checkCartEquality();
  }, [cartData, checkoutArr]);

  useEffect(() => {
    let calculatedTotal = 0;
    checkoutArr.forEach((item) => {
      const product = productsData.find((product) => product.name === item.productName);
      const subtotal = product.price * item.quantity;
      calculatedTotal += subtotal;
    });
    setTotalAmount(calculatedTotal);
  }, [checkoutArr, productsData]);



  return (
    <>
      <div id='checkout'>
      <Container className='p-3'>
      <h1 className='text-center'>Checkout</h1>
      {(cart === null && address === null) ? ( // Display "Loading..." while fetching cart data
        <div className="text-center">
          <h1> Loading... </h1>
        </div>
      ) : (
        <>
          <Container className="text-center">{cardElements}</Container>

          <Row>
            <Col className="text-center mt-3">
              <h5>Total Amount: ₱{totalAmount.toFixed(2)}</h5>
              {Object.keys(fetchedVoucher).length !== 0?
                 (fetchedVoucher.type === 'amount')?
                  <h5>Total Amount Due: ₱{(totalAmount-fetchedVoucher.value).toFixed(2)}</h5>
                  :
                  <h5>Total Amount Due: ₱{(totalAmount*(1-fetchedVoucher.value/100)).toFixed(2)}</h5>
                :
                <h5></h5>
              }
            </Col>
          </Row>

          <Container>
          <Row className="text-center mt-3">
            <Col>
            <Form.Group controlId="modeOfPaymentSelect">
              <Form.Label><h5>Select Mode of Payment:</h5></Form.Label>
              <Form.Control
                className='text-center'
                as="select"
                value={modeOfPayment}
                onChange={(e) => setModeOfPayment(e.target.value)}
              >
                <option>Cash on Delivery</option>
                <option>Credit Card</option>
                <option>Debit Card</option>
                <option>Mobile Wallet</option>
              </Form.Control>
            </Form.Group>
            </Col>
          </Row>
          </Container>

          <Container>
            <Row className="text-center mt-3">
              <Form.Group controlId="voucherInput">
                <Row>
                  <Col>
                    <Form.Label><h5>Voucher:</h5></Form.Label>
                  </Col>
                </Row>
                <Row>
                  <Col className='col-10'>
                    {Object.keys(fetchedVoucher).length !== 0?
                    <Form.Control
                      className='text-center'
                      type='text'
                      placeholder='Input Voucher Code'
                      name='voucher'
                      value={voucher}
                      onChange={(e) => setVoucher(e.target.value)}
                      disabled
                    >
                    </Form.Control>
                    :
                    <Form.Control
                      className='text-center'
                      type='text'
                      placeholder='Input Voucher Code'
                      name='voucher'
                      value={voucher}
                      onChange={(e) => setVoucher(e.target.value)}
                    >
                    </Form.Control>
                    }
                  </Col>
                  <Col className='col-2 d-flex align-items-center'>
                    {Object.keys(fetchedVoucher).length !== 0?
                    <Button variant='secondary' size='sm' disabled>Applied</Button>
                    :
                    <Button variant='success' size='sm' onClick={ () => applyVoucher()}>Apply</Button>
                    }
                  </Col>
                </Row>
              </Form.Group>
            </Row>
          </Container>

          <Row className='m-3'>
              <Col className='text-center'>
                <h5>Address Details:</h5>
              </Col>
            </Row>
            <Row className='text-center'>
              <Col className='col-12'>
                <p>Region: {`${address.region}`}</p>
              </Col>
              <Col className='col-12'>
                <p>Province: {`${address.province}`}</p>
              </Col>
              <Col className='col-12'>
                <p>City: {`${address.city}`}</p>
              </Col>
              <Col className='col-12'>
                <p>Barangay: {`${address.barangay}`}</p>
              </Col>
              <Col className='col-12'>
                <p>Street Name: {`${address.streetName}`}</p>
              </Col>
              <Col className='col-12'>
                <p>Building/House Number: {`${address.buildingHouseNumber}`}</p>
              </Col>
              <Col>
                <Button onClick={handleNavigateToProfile}>Edit Address</Button>
              </Col>
            </Row>

          <div style={centerButtonStyle} className='mb-3'>
            {!isEqual ? (
              <Button className="mx-3" variant="success" onClick={createOrder}>
                SEND ORDER
              </Button>
            ) 
            : 
            (
              <Button className="mx-3" variant="primary" onClick={checkoutCart}>
              SEND ORDER
              </Button>
            )}
            <Button className="mx-3" variant="warning" onClick={handleCancel}>
              Cancel
            </Button>
          </div>
        </>
      )}
      </Container>
      </div>
    </>
  );
}

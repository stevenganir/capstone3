import React, { useState, useContext } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function EditProduct({ product, fetchData }) {

  const { user, setUser } = useContext(UserContext);

  const [productId, setProductId] = useState('');

  //Forms state
  //Add state for the forms of course
  const [name, setName] = useState('');
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState('')
  const [stockQuantity, setStockQuantity] = useState('')
  const [imageUrl, setImageUrl] = useState('')

  //state for editCourse Modals to open/close
  const [showEdit, setShowEdit] = useState(false)

  //function for opening the modal
  const openEdit = (productId) => {
    //to still get the actual data from the form
    fetch(`https://capstone2-ganir.onrender.com/product/get-product/${ productId }`)
    .then(res => res.json())
    .then(data => {
      //populate all the input values with course info that we fetched
      setProductId(data._id);
      setName(data.name);
      setDescription(data.description);
      setPrice(data.price)
      setStockQuantity(data.stockQuantity)
      setImageUrl(data.imageUrl)
    })

    //Then, open the modal
    setShowEdit(true)
  }

  const closeEdit = () => {
    setShowEdit(false);
    setName('')
    setDescription('')
    setPrice(0)
    setStockQuantity(0)
    setImageUrl('')
  }


  //function to update the course
    const editProduct = async (e, productId) => {
      e.preventDefault();

      try{ 
        const response = await fetch(`https://capstone2-ganir.onrender.com/product/update-product/${ productId }`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${user.token}`
          },
          body: JSON.stringify({
            name: name,
            description: description,
            price: price,
            stockQuantity: stockQuantity,
            imageUrl: imageUrl
          })
        })
      
        const data = await response.json()

        if(response.ok) {
          Swal.fire({
            text: `${data.message}`,
            icon: 'success',
          });
          closeEdit();
          fetchData();
          
        } else {
          Swal.fire({
            text: `${data.message}`,
            icon: 'error'
          });
          closeEdit();
          fetchData();
        }
      }catch (error) {
      console.error('Error:', error);
      fetchData();
      }
    }


  return(
    <>
      <Button variant="primary" size="sm" onClick={() => openEdit(product)}> Edit </Button>

    {/*Edit Modal Forms*/}
      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={e => editProduct(e, productId)}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Product</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control 
                type="text" 
                value={name} 
                onChange={e => setName(e.target.value)} 
                required/>
            </Form.Group>

            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control 
                as='textarea'
                rows={3}
                value={description} 
                onChange={e => setDescription(e.target.value)} 
                required/>
            </Form.Group>

            <Form.Group>
              <Form.Label>Price</Form.Label>
              <Form.Control 
                type="number" 
                value={price} 
                onChange={e => setPrice(e.target.value)} 
                required/>
            </Form.Group>

            <Form.Group>
              <Form.Label>Stock Quantity</Form.Label>
              <Form.Control 
                type="number" 
                value={stockQuantity} 
                onChange={e => setStockQuantity(e.target.value)} 
                required/>
            </Form.Group>

            <Form.Group>
              <Form.Label>Image URL</Form.Label>
              <Form.Control 
                type="text" 
                value={imageUrl} 
                onChange={e => setImageUrl(e.target.value)} 
                required/>
            </Form.Group>

            <p className='mt-2'>Image Preview:</p>
            <img style={{ width: '100%' }} src={imageUrl}/>

          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeEdit}>Close</Button>
            <Button variant="success" type="submit">Submit</Button>
          </Modal.Footer>
        </Form>
        
      </Modal>
    </>
    )
}
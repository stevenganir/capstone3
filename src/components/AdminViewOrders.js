import React, { useEffect, useState, useContext } from 'react';
import { Table, Modal, Button, DropdownButton, Dropdown, Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AdminViewOrders({ orders, fetchOrders }) {
  const { user } = useContext(UserContext);

  const [allOrders, setAllOrders] = useState([]);
  const [loading, setLoading] = useState(true);
  const [showProductModal, setShowProductModal] = useState(false);
  const [showShippingModal, setShowShippingModal] = useState(false);
  const [modalProductData, setModalProductData] = useState([]);
  const [modalShippingData, setModalShippingData] = useState({});

  // Maintain an array of selected statuses
  const [selectedStatuses, setSelectedStatuses] = useState({});

  const [orderId, setOrderId] = useState(null);

  const openProductModal = (products) => {
    setShowProductModal(true);
    setModalProductData(products);
  };

  const openShippingModal = (shippingDetails) => {
    setShowShippingModal(true);
    setModalShippingData(shippingDetails);
  };

  const closeModal = () => {
    setShowProductModal(false);
    setShowShippingModal(false);
  };

  const handleStatusChange = async (status, orderId) => {
    // Update the status for the specific order
    setSelectedStatuses((prevStatuses) => ({
      ...prevStatuses,
      [orderId]: status,
    }));

    setOrderId(orderId);

    switch (status) {
      case 'processing':
        await updateOrderStatusToProcessing(orderId);
        break;
      case 'shipped':
        await updateOrderStatusToShipped(orderId);
        break;
      case 'out for delivery':
        await updateOrderStatusToOutForDelivery(orderId);
        break;
      case 'delivered':
        await updateOrderStatusToDelivered(orderId);
        break;
      case 'cancelled':
        await updateOrderStatusToCancelled(orderId);
        break;
      case 'completed':
        await updateOrderStatusToCompleted(orderId);
        break;
      default:
        // Handle the default case or an unknown status
        break;
    }
  };

  const updateOrderStatusToProcessing = async (orderId) => {
  try {
    const response = await fetch('https://capstone2-ganir.onrender.com/order/update-status/processing', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`
      },
      body: JSON.stringify({ orderId: orderId }),
    });

    if (response.ok) {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'success',
      });
      fetchOrders(); // Refresh the orders data after status update
    } else {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'error',
      });
      fetchOrders(); // Refresh the orders data after status update
    }
  } catch (error) {
    // Handle network errors or other issues
    Swal.fire({
      title: 'Error',
      icon: 'error',
      text: 'An error occurred while communicating with the server.'
    });
  }
};

const updateOrderStatusToShipped = async (orderId) => {
  try {
    const response = await fetch('https://capstone2-ganir.onrender.com/order/update-status/shipped', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`
      },
      body: JSON.stringify({ orderId: orderId }),
    });

    if (response.ok) {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'success',
      });
      fetchOrders(); // Refresh the orders data after status update
    } else {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'error',
      });
      fetchOrders(); // Refresh the orders data after status update
    }
  } catch (error) {
    // Handle network errors or other issues
    Swal.fire({
      title: 'Error',
      icon: 'error',
      text: 'An error occurred while communicating with the server.'
    });
  }
};

const updateOrderStatusToOutForDelivery = async (orderId) => {
  try {
    const response = await fetch('https://capstone2-ganir.onrender.com/order/update-status/outForDelivery', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`
      },
      body: JSON.stringify({ orderId: orderId }),
    });

    if (response.ok) {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'success',
      });
      fetchOrders(); // Refresh the orders data after status update
    } else {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'error',
      });
      fetchOrders(); // Refresh the orders data after status update
    }
  } catch (error) {
    // Handle network errors or other issues
    Swal.fire({
      title: 'Error',
      icon: 'error',
      text: 'An error occurred while communicating with the server.'
    });
  }
};

const updateOrderStatusToDelivered = async (orderId) => {
  try {
    const response = await fetch('https://capstone2-ganir.onrender.com/order/update-status/delivered', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`
      },
      body: JSON.stringify({ orderId: orderId }),
    });

    if (response.ok) {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'success',
      });
      fetchOrders(); // Refresh the orders data after status update
    } else {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'error',
      });
      fetchOrders(); // Refresh the orders data after status update
    }
  } catch (error) {
    // Handle network errors or other issues
    Swal.fire({
      title: 'Error',
      icon: 'error',
      text: 'An error occurred while communicating with the server.'
    });
  }
};

const updateOrderStatusToCancelled = async (orderId) => {
  try {
    const response = await fetch('https://capstone2-ganir.onrender.com/order/update-status/cancelled', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`
      },
      body: JSON.stringify({ orderId: orderId }),
    });

    if (response.ok) {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'success',
      });
      fetchOrders(); // Refresh the orders data after status update
    } else {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'error',
      });
      fetchOrders(); // Refresh the orders data after status update
    }
  } catch (error) {
    // Handle network errors or other issues
    Swal.fire({
      title: 'Error',
      icon: 'error',
      text: 'An error occurred while communicating with the server.'
    });
  }
};

const updateOrderStatusToCompleted = async (orderId) => {
  try {
    const response = await fetch('https://capstone2-ganir.onrender.com/order/update-status/completed', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`
      },
      body: JSON.stringify({ orderId: orderId }),
    });

    if (response.ok) {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'success',
      });
      fetchOrders(); // Refresh the orders data after status update
    } else {
      const data = await response.json();
      Swal.fire({
        title: `${data.message}`,
        icon: 'error',
      });
      fetchOrders(); // Refresh the orders data after status update
    }
  } catch (error) {
    // Handle network errors or other issues
    Swal.fire({
      title: 'Error',
      icon: 'error',
      text: 'An error occurred while communicating with the server.'
    });
  }
};

  useEffect(() => {
    const ordersArr = orders.map((order, index) => {
      return (
        <tr key={order._id}>
          <td>{index + 1}</td>
          <td>{order._id}</td>
          <td>{order.userId}</td>
          <td>
            <Button
              variant="info"
              onClick={() => openProductModal(order.products)}
            >
              View Products
            </Button>
          </td>
          <td>₱{order.totalAmount}</td>
          <td>₱{order.voucherDiscount}</td>
          <td>₱{order.totalAmountDue}</td>
          <td>{order.modeOfPayment}</td>
          <td>
            <Button
              variant="info"
              onClick={() => openShippingModal(order.shippingDetails)}
            >
              View Address
            </Button>
          </td>
          <td>{order.purchasedOn}</td>
          <td>
            <DropdownButton
              title={
                selectedStatuses[order._id] || order.orderStatus // Use the individual order's status
              }
              variant="success"
              onSelect={(status) => handleStatusChange(status, order._id)}
            >
              <Dropdown.Item eventKey="processing">Processing</Dropdown.Item>
              <Dropdown.Item eventKey="shipped">Shipped</Dropdown.Item>
              <Dropdown.Item eventKey="out for delivery">
                Out for Delivery
              </Dropdown.Item>
              <Dropdown.Item eventKey="delivered">Delivered</Dropdown.Item>
              <Dropdown.Item eventKey="cancelled">Cancelled</Dropdown.Item>
              <Dropdown.Item eventKey="completed">Completed</Dropdown.Item>
            </DropdownButton>
          </td>
        </tr>
      );
    });

    setAllOrders(ordersArr);
  }, [orders, selectedStatuses]);

  return (
    <>
    <div id='adminvieworders'>
    <Container fluid className="p-3 text-center">
      <Row>
      <Col>
      <h1 className="text-center my-4">All Orders</h1>

      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>#</th>
            <th>Order ID</th>
            <th>User ID</th>
            <th>Ordered Products</th>
            <th>Total Amount</th>
            <th>Voucher Discount</th>
            <th>Total Amount Due</th>
            <th>Mode of Payment</th>
            <th>Shipping Details</th>
            <th>Purchased on</th>
            <th>Status</th>
          </tr>
        </thead>

        <tbody>{allOrders}</tbody>
      </Table>
      </Col>
      </Row>
    </Container>
    </div>

      <Modal show={showProductModal} onHide={closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>Products</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ul>
            {modalProductData.map((product, index) => (
              <li key={index}>
                Product ID: {product.productId}<br />
                Product Name: {product.productName}<br />
                Unit Price: ₱{product.unitPrice}<br />
                Quantity: {product.quantity}<br />
                Subtotal: ₱{product.subtotal}<br />
                <hr />
              </li>
            ))}
          </ul>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showShippingModal} onHide={closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>Shipping Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Region: {modalShippingData.region}</p>
          <p>Province: {modalShippingData.province}</p>
          <p>City: {modalShippingData.city}</p>
          <p>Barangay: {modalShippingData.barangay}</p>
          <p>Street Name: {modalShippingData.streetName}</p>
          <p>Building/House Number: {modalShippingData.buildingHouseNumber}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

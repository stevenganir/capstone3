import { useState, useEffect, useContext } from 'react'
import { Card, Container, Row, Col, Form, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'

import Checkout from './Checkout'
import UserContext from '../UserContext'
import Banner from '../components/Banner'


export default function CartView({ fetchCart, cartData, productsData, fetchProductsData }) {

	const [cartItems, setCartItems] = useState([])
	const { user } = useContext(UserContext)
	const [checkoutArr, setCheckoutArr] = useState([])

	const bannerData = {
    title: "Cart Empty",
    content: "Start Browsing Our Products!",
    destination: "/menu",
    label: "Shop now!",
    textColor: 'white'
}

	const updateCartItemQuantity = async (productName, newQuantity) =>{
		try {
    		const response = await fetch('https://capstone2-ganir.onrender.com/cart/updateQuantity', {
      		method: 'PUT',
      		headers: {
        	'Content-Type': 'application/json',
        	Authorization: `Bearer ${user.token}`
      		},
      		body: JSON.stringify({ productName, newQuantity }),
    		});

	    	if (response.ok) {
	      		fetchCart()
	    	} 
	    	else {
	      		// Handle error
	      		console.error('Error updating quantity:', response.statusText);
	    	}
  		} 
  		catch (error) {
    		console.error('Error:', error);
  		}
	};

	const deleteCartItem = async (productName) =>{
		try {
    		const response = await fetch('https://capstone2-ganir.onrender.com/cart/deleteCartItem', {
      		method: 'DELETE',
      		headers: {
        	'Content-Type': 'application/json',
        	Authorization: `Bearer ${user.token}`
      		},
      		body: JSON.stringify({ productName }),
    		});

	    	if (response.ok) {
	      		Swal.fire({
       				
        			icon: 'error',
        			text: `${productName} removed from cart`
      			});
	      		fetchCart()
	    	} 
	    	else {
	      		// Handle error
	      		console.error('Error removing item from cart', response.statusText);
	    	}
  		} 
  		catch (error) {
    		console.error('Error:', error);
  		}
	};

	function handleRemoveItem(item) {
  		deleteCartItem(item.productName)
	}

	function handleDecrement(item) {
  		const newQuantity = item.quantity - 1;
  		if (newQuantity >= 1) {
    		updateCartItemQuantity(item.productName, newQuantity);    	
  		}
	}

	function handleIncrement(item) {
  		const newQuantity = item.quantity + 1;
  			updateCartItemQuantity(item.productName, newQuantity);
	}

	function handleCheckout() {
  		const checkout = cartData.cart.addedProducts.map(item => ({
    		productName: item.productName, // Assuming 'name' is the property that stores the product name
    		quantity: item.quantity, // Assuming 'quantity' is the property that stores the quantity
  		}));

  		// Set the checkoutArr state with the new array
  		setCheckoutArr(checkout); // Assuming you have a state variable 'checkoutArr' and a 'setCheckoutArr' function to update it

	}


	

	

	useEffect(() => {
		const cartItemsArr = cartData.cart.addedProducts.map(item => {
			const product = productsData.find((product) => product.name === item.productName);
			//only render the active courses
				return (
					<Container fluid key={item._id}>
						<Card className="mt-3">
	           	 			<Card.Body>
	           	 				<Row>
	           	 					<Col className="col-6 d-flex align-items-center justify-content-center">
				                		<Card.Img src={product ? product.imageUrl : ''} style={{ maxHeight: '240px', maxWidth: '240px' }} alt={item.productName} />
				               		</Col>
				               		<Col className="col-6 text-center">
				                		<h3>{item.productName}</h3>
				                		<h4>Price:</h4>
                    					<h5>₱{item.unitPrice}</h5>
                    					<h4>Quantity:</h4>
                						<div className="d-flex justify-content-center align-items-center">
                  							<Button variant='warning' size='sm' onClick={() => handleDecrement(item)}>-</Button>
                  							<span className="mx-2">{item.quantity}</span>
                  							<Button variant='success' size='sm' onClick={() => handleIncrement(item)}>+</Button>
                						</div>
                						<Button className='mt-2' size='sm' variant='danger' onClick={() => handleRemoveItem(item)}>Remove Item</Button>
				               		</Col>
				               	</Row>
	            			</Card.Body>
	        			</Card>
	        		</Container>
				)
		})

		setCartItems(cartItemsArr)

	}, [cartData, productsData])

	useEffect(()=>{
		if(cartData.cart.addedProducts.length === 0){
			setCheckoutArr([])
		}
	}, [cartData])




	return(
		<>
			{(cartData.cart.addedProducts.length ===0)?
				<>
					<div id='cartempty'>
					<Banner bannerData={bannerData} />
					</div>
				</>
				:
				 (checkoutArr.length ===0) 
	        	?
	        		<>
	        			<div id='cartview'>
			         		<Container className='p-3'>
			         			<Row className='text-center'>
			         				<Col>
			         					<h1 style={{color: 'white', fontSize: '4rem', fontWeight:'bold'}}>Cart</h1>
			         				</Col>
			         			</Row>
			         			<Row>
			         			<Col className='d-flex align-items-center justify-content-center'>
								{cartItems}
								</Col>
								</Row>
								<Row>
								<Col className='text-center mt-2 p-3'>
									<h2 style={{color: 'white'}}>Total Amount: ₱{cartData.cart.totalPrice}</h2>
									<Button variant='primary' className='mt-2'  onClick={() => handleCheckout()}>Checkout</Button>
								</Col>
								</Row>
							</Container>
						</div>
	          		</>
	        	:
	          		<>

	          		<Checkout cartData={cartData} checkoutArr={checkoutArr} setCheckoutArr={setCheckoutArr} productsData={productsData}/>
	          		</>
	      		
	      	}
		</>
	)



}
import { useState, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import {Container, Navbar, Nav} from 'react-bootstrap'

import UserContext from '../UserContext';


export default function AppNavbar() {

  const { user } = useContext(UserContext)

  return (
    <Navbar bg="dark" data-bs-theme="dark" expand="lg" className="sticky-top px-5">
        <Container fluid>
          <Navbar.Brand id='navbar-brand' as={Link} to='/'>Out of the Oven</Navbar.Brand>

          <Navbar.Toggle aria-controls="basic-navbar-nav"/>
          <Navbar.Collapse id='basic-navbar-nav'>
          <Nav className="ms-auto">
            
            {(user.token !== null) 

            ?

              user.isAdmin 
              ?
              <>
              <Nav.Link as={Link} to='/menu'>Edit Menu</Nav.Link>
              <Nav.Link as={Link} to='/createproduct'>Create Product</Nav.Link>
              <Nav.Link as={Link} to='/orders'>Orders</Nav.Link>
              <Nav.Link as={Link} to='/vouchers'>Vouchers</Nav.Link>
              <Nav.Link as={Link} to='/users'>Users</Nav.Link>
              <Nav.Link as={Link} to='/profile'>Profile</Nav.Link>
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
              </>
              :
              <>
              <Nav.Link as={Link} to='/menu'>Menu</Nav.Link>
              <Nav.Link as={Link} to='/cart'>Cart</Nav.Link>
              <Nav.Link as={Link} to='/orders'>My Orders</Nav.Link>
              <Nav.Link as={Link} to='/profile'>Profile</Nav.Link>
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
              </>
            :
              <>
              <Nav.Link as={Link} to='/menu'>Menu</Nav.Link>
              <Nav.Link as={Link} to='/login'>Login</Nav.Link>
              <Nav.Link as={Link} to='/register'>Register</Nav.Link>
              </>
          }

          </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
  );
}


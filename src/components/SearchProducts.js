import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function SearchProduct({ setSearchResults }) {
  const [formData, setFormData] = useState({
    name: '',
    minPrice: '',
    maxPrice: '',
  });

  const [isActive, setIsActive] = useState(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleClearSearch = (event) => {
    setSearchResults([]);
    setFormData({
      name: '',
      minPrice: '',
      maxPrice: '',
    });
  };

  const handleSearch = async () => {
    const { name, minPrice, maxPrice } = formData;

    // Ensure at least one of the search parameters is provided
    if (!name && !minPrice && !maxPrice) {
      alert('Please provide at least one search parameter.');
      return;
    }

    try {
      const response = await fetch('https://capstone2-ganir.onrender.com/product/get-products/filter', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name,
          minPrice: Number(minPrice),
          maxPrice: Number(maxPrice),
        }),
      });

      const data = await response.json();

      if (response.ok) {
        setSearchResults(data);
      } else {
        Swal.fire({
          text: `${data.message}`,
          icon: 'error',
        });
      }
    } catch (error) {
      console.error('Error:', error.message);
    }
  };

  useEffect(() => {
    if (formData.name !== '' || formData.minPrice !== '' || formData.maxPrice !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [formData.name, formData.minPrice, formData.maxPrice]);

  return (
    <Form>
      <Row className="mb-3">
        <Col sm={12} className="text-center">
          <Form.Group controlId="productName">
            <Form.Label >Product Name</Form.Label>
            <Form.Control
              className="text-center"
              type="text"
              name="name"
              value={formData.name}
              onChange={handleInputChange}
            />
          </Form.Group>
        </Col>
      </Row>

      <Row className="mb-3">
        <Col sm={6} className="text-center">
          <Form.Group controlId="minPrice">
            <Form.Label >Minimum Price</Form.Label>
            <Form.Control
              className="text-center"
              type="number"
              name="minPrice"
              value={formData.minPrice}
              onChange={handleInputChange}
            />
          </Form.Group>
        </Col>
        <Col sm={6} className="text-center">
          <Form.Group controlId="maxPrice">
            <Form.Label >Maximum Price</Form.Label>
            <Form.Control
              className="text-center"
              type="number"
              name="maxPrice"
              value={formData.maxPrice}
              onChange={handleInputChange}
            />
          </Form.Group>
        </Col>
      </Row>

      <div style={{ textAlign: 'center', marginTop: '20px' }}>
        {isActive ? (
          <Button variant="primary" onClick={handleSearch}>
            Search
          </Button>
        ) : (
          <Button variant="primary" disabled>
            Search
          </Button>
        )}
        <Button variant="secondary" onClick={handleClearSearch} style={{ marginLeft: '10px' }}>
          Clear Filters
        </Button>
      </div>
    </Form>
  );
}

import React, { useEffect, useState } from 'react';
import { Container, Table, Modal, Button } from 'react-bootstrap';

import ArchiveProduct from './ArchiveProduct';
import EditProduct from './EditProduct';

export default function AdminView({ productsData, fetchData }) {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedProduct, setSelectedProduct] = useState(null);

  useEffect(() => {
    const productsArr = productsData.map((product) => {
      return (
        <tr key={product._id}>
          <td>{product._id}</td>
          <td>{product.name}</td>
          <td className="description-cell">
            {/* Center the button horizontally and vertically */}
            <div className="d-flex align-items-center justify-content-center">
              <Button
                variant="info"
                onClick={() => setSelectedProduct(product)}
              >
                View Description
              </Button>
            </div>
          </td>
          <td>₱{product.price}</td>
          <td>{product.stockQuantity}</td>
          <td className={product.isActive ? 'text-success' : 'text-danger'}>
            {product.isActive ? 'Available' : 'Unavailable'}
          </td>
          <td>
            {' '}
            <EditProduct product={product._id} fetchData={fetchData} />{' '}
          </td>
          <td>
            <ArchiveProduct
              product={product._id}
              isActive={product.isActive}
              fetchData={fetchData}
            />
          </td>
        </tr>
      );
    });

    setProducts(productsArr);
  }, [productsData]);

  return (
    <div id='adminview'>
    <Container fluid className="p-3">
      <h1 className="text-center">Edit Menu</h1>

      <Table striped bordered hover responsive className="mt-4">
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Stocks</th>
            <th>Availability</th>
            <th colSpan="2">Actions</th>
          </tr>
        </thead>

        <tbody className="text-center">{products}</tbody>
      </Table>

      {/* Description Modal */}
      <Modal
        show={!!selectedProduct}
        onHide={() => setSelectedProduct(null)}
      >
        <Modal.Header closeButton>
          <Modal.Title>Description</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {selectedProduct && selectedProduct.description}
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setSelectedProduct(null)}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
    </div>
  );
}

import { useContext, useEffect, useState } from 'react';
import { Button, Modal, Table, Container } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function UserViewOrders() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);
  const [allOrders, setAllOrders] = useState([]);
  const [showProductModal, setShowProductModal] = useState(false);
  const [showShippingModal, setShowShippingModal] = useState(false);
  const [modalProductData, setModalProductData] = useState([]);
  const [modalShippingData, setModalShippingData] = useState({});

  const fetchUserOrders = async () => {
    try {
      const response = await fetch('https://capstone2-ganir.onrender.com/order/user-orders', {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
      });

      if (response.ok) {
        const data = await response.json();

        if (data) {
          setOrders(data);
        }
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const openProductModal = (products) => {
    setShowProductModal(true);
    setModalProductData(products);
  };

  const openShippingModal = (shippingDetails) => {
    setShowShippingModal(true);
    setModalShippingData(shippingDetails);
  };

  const closeModal = () => {
    setShowProductModal(false);
    setShowShippingModal(false);
  };

  useEffect(() => {
    fetchUserOrders();
  }, []); // Empty dependency array to fetch orders once when the component mounts

  useEffect(() => {
    // Check if orders have been loaded before mapping
    if (orders.length > 0) {
      const ordersArr = orders.map((order, index) => {
        return (
          <tr key={order._id}>
            <td>{index + 1}</td>
            <td>{order._id}</td>
            <td>{order.purchasedOn}</td>
            <td >
              {/* Display this column on medium screens and larger */}
              <Button
                variant="info"
                onClick={() => openProductModal(order.products)}
              >
                View Products
              </Button>
            </td>
            <td>₱{order.totalAmount}</td>
            <td>₱{order.voucherDiscount}</td>
            <td>₱{order.totalAmountDue}</td>
            <td>{order.modeOfPayment}</td>
            <td >
              {/* Display this column on large screens and larger */}
              <Button
                variant="info"
                onClick={() => openShippingModal(order.shippingDetails)}
              >
                View Address
              </Button>
            </td>
            <td>{order.orderStatus}</td>
          </tr>
        );
      });

      setAllOrders(ordersArr);
    }
  }, [orders]);

  return (
    <>
      <div id='uservieworders'>
      <Container fluid className='p-3'>
      <h1 className="text-center">My Orders</h1>

      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>#</th>
            <th>Order ID</th>
            <th>Purchased on</th>
            <th >Ordered Products</th>
            <th>Total Amount</th>
            <th>Voucher Discount</th>
            <th>Total Amount Due</th>
            <th>Mode of Payment</th>
            <th >Shipping Details</th>
            <th>Status</th>
          </tr>
        </thead>

        <tbody className="text-center">{allOrders}</tbody>
      </Table>

      <Modal show={showProductModal} onHide={closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>Products</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ul>
            {modalProductData.map((product, index) => (
              <li key={index}>
                Product ID: {product.productId}<br />
                Product Name: {product.productName}<br />
                Unit Price: ₱{product.unitPrice}<br />
                Quantity: {product.quantity}<br />
                Subtotal: ₱{product.subtotal}<br />
                <hr />
              </li>
            ))}
          </ul>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showShippingModal} onHide={closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>Shipping Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Region: {modalShippingData.region}</p>
          <p>Province: {modalShippingData.province}</p>
          <p>City: {modalShippingData.city}</p>
          <p>Barangay: {modalShippingData.barangay}</p>
          <p>Street Name: {modalShippingData.streetName}</p>
          <p>Building/House Number: {modalShippingData.buildingHouseNumber}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      </Container>
      </div>
    </>
  );
}
